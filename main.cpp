/* Модели и методы анализа проектных решений. Лабораторная работа #4. МКЭ*/
/* Макаренко Н.А. РК6-74Б Вариант-83                                     */

/* Главная управляющая функция, осуществляющая расчет */

#include <iostream>
#include <vector>
#include <cmath>
#include "const.h"

using std::vector;

vector<double> MKELinear();
vector<double> MKECubic();
vector<double> CalcYReal(vector<double> &x_vec);
double calcEps(const vector<double> &y_real, const vector<double> &y);

int main()
{
    // Подготовка вектора узлов, по которым будет вестись расчет
    vector<double> x(ELEMS_NUM + 1);
	for (unsigned i = 0; i < x.size(); i++)
	{
		x.at(i) = X_BEGIN + (double)i * L;
	}
	unsigned x_size = x.size();

    // Решение диф. уравнения по МКЭ (с использованием либо линейных, либо кубических КЭ)
	#ifdef LINEAR_MODE
	    vector<double> y = MKELinear();
	#else
	    vector<double> y = MKECubic();
    #endif

    // Аналитическое решение диф. уравнения на тех же узлах
	vector<double> y_real = CalcYReal(x);

    // Подготовка графиков аналитического и численного решений
	FILE* gp = popen("gnuplot -persist", "w");

	fprintf(gp, "$mke << EOD\n");
	for (unsigned i = 0; i < x_size; i++)
	{
		fprintf(gp, "%lf %lf\n", x.at(i), y.at(i));
	}
	fprintf(gp, "EOD\n");

	fprintf(gp, "$real << EOD\n");
	for (unsigned i = 0; i < x_size; i++)
	{
		fprintf(gp, "%lf %lf\n", x.at(i), y_real.at(i));
	}
	fprintf(gp, "EOD\n");

	fprintf(gp, "set grid\n");
	fprintf(gp, "plot '$mke' using 1:2 with lp lc '#0000ff' lw 1.5 pt 7 ps 0.5 title 'МКЭ',"
                "'$real' using 1:2 with lines lc rgb '#ff0000' lt 1 lw 2 title 'Аналитическое',\n");

    // Диагностика результатов расчета
    for (unsigned i = 0; i < x_size; i++)
    {
        printf("#%2u:\tx = %e\ty_real = %e\ty_mke = %e\teps = %e\n\n", i, x.at(i), y.at(i), y_real.at(i),
               fabs(y.at(i) - y_real.at(i)));
    }

    #ifdef LINEAR_MODE
        printf("Линейные КЭ\n");
    #else
        printf("Кубические КЭ\n");
    #endif
    printf("Количество элементов: %d\n", ELEMS_NUM);
	printf("Максимальная погрешность: %e\n", calcEps(y_real, y));

    return 0;
}
