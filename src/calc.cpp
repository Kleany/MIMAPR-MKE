/* Модели и методы анализа проектных решений. Лабораторная работа #4. МКЭ*/
/* Макаренко Н.А. РК6-74Б Вариант-83                                     */

/* Вспомогательные функции расчета */

#include <vector>
#include <cmath>

using std::vector;

// Аналитическое решение диф. уравнения
double YReal(double x)
{
    double C2 = (-11.0/6.0) / ((-6.0/5.0)*exp(-48.0/5.0) + 1 - exp(-48.0/5.0));
    double C1 = -C2 - 10;
    double y = C1 + C2*exp((-6.0/5.0)*x) + (7.0/6.0)*x;
    return y;
}

// Функция вычисления аналитического решения в заданных узлах
vector<double> CalcYReal(vector<double>& x_vec)
{
    unsigned x_vec_size = x_vec.size();
    vector<double> y_vec = vector<double>(x_vec_size);
    for (unsigned i = 0; i < x_vec_size; i++)
    {
        y_vec.at(i) = YReal(x_vec.at(i));
    }
    return y_vec;
}

// Функция расчета максимальной погрешности
double calcEps(const vector<double>& y_real, const vector<double>& y)
{
    double max_err = 0.0;
    for (unsigned i = 0; i < y_real.size(); i++)
    {
        double err = fabs(y_real.at(i) - y.at(i));
        if (err > max_err)
        {
            max_err = err;
        }
    }
    return max_err;
}
