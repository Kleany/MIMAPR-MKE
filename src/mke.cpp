/* Модели и методы анализа проектных решений. Лабораторная работа #4. МКЭ*/
/* Макаренко Н.А. РК6-74Б Вариант-83                                     */

/* Функции МКЭ расчета */

#include <vector>
#include <cmath>
#include "const.h"

using std::vector;

// Функция решения СЛАУ Ax=b методом Гаусса
vector<double> GaussSolve(vector<vector<double>>& A, vector<double>& b)
{
    unsigned row_size = A.size();
    unsigned col_size = A.back().size();

    // Прямой ход
    double pivot;
    for (unsigned i = 0; i < row_size; i++)
    {
        for (unsigned j = i + 1; j < col_size; j++)
        {
            if (fabs(A.at(j).at(i)) < EPS_GAUSS)
            {
                continue;
            }
            pivot = A.at(j).at(i) / A.at(i).at(i);
            b.at(j) -= pivot * b.at(i);
            for (unsigned k = 0; k < row_size; k++)
            {
                A.at(j).at(k) -= pivot * A.at(i).at(k);
            }
        }
    }

    // Обратный ход
    vector<double> x(row_size);
    for (int i = (int)row_size - 1; i >= 0; i--)
    {
        x.at(i) = b.at(i);
        for (unsigned j = i + 1; j < row_size; j++)
        {
            x.at(i) -= x.at(j) * A.at(i).at(j);
        }
        x.at(i) /= A.at(i).at(i);
    }
    return x;
}

// МКЭ расчет с использованием линейных КЭ
vector<double> MKELinear()
{
    unsigned size = ELEMS_NUM + 1;
    vector<vector<double>> A(size, vector<double>(size));
    vector<double> b(size);

    // Локальная матрица жесткости для линейного КЭ
    vector<vector<double>> local_matrix = {
            { a/L - C * L/3.0 + B*1.0/2.0, -a/L - C * L/6.0 - B*1.0/2.0},
            { -a/L - C * L/6.0 + B*1.0/2.0, a/L - C*L/3.0 - B*1.0/2.0},
    };

    // Ансамблирование и получение глобальной матрицы жесткости для линейного КЭ
    for (unsigned i = 0; i < ELEMS_NUM; i++)
    {
        for (unsigned j = 0; j < 2; j++)
        {
            for (unsigned k = 0; k < 2; k++)
            {
                A.at(i + j).at(i + k) += local_matrix.at(j).at(k);
            }
        }
    }
    for (unsigned i = 0; i < size ; i++)
    {
        b.at(i) = D * L;
    }

    // Учет ГУ
    // Начало интервала
    if (BEGIN_TYPE == TYPE_1)
    {
        b.at(0) = BEGIN;
        A.at(0).at(0) = 1;
        A.at(0).at(1) = 0;
    }
    else if (BEGIN_TYPE == TYPE_2)
    {
        b.at(0) = D * L/2 - a*BEGIN;
    }
    else if (BEGIN_TYPE == TYPE_3)
    {
        b.at(0) = D * L/2;
        A.at(0).at(0) -= a*BEGIN;
    }

    // Конец интервала
    if (END_TYPE == TYPE_1)
    {
        b.at(size-1) = END;
        A.at(size-1).at(size-1) = 1;
        A.at(size-1).at(size-2) = 0;
    }
    else if (END_TYPE == TYPE_2)
    {
        b.at(size-1) = D * L/2 + a*END;
    }
    else if (END_TYPE == TYPE_3)
    {
        b.at(size-1) = D * L/2;
        A.at(size-1).at(size-1) -= a*END;
    }

    //Решение полученной СЛАУ методом Гаусса
    vector<double> res = GaussSolve(A, b);
    return res;
}

vector<double> MKECubic()
{
    unsigned size = ELEMS_NUM + 1;
    vector<vector<double>> A(size,vector<double>(size));
    vector<double> b(size);

    // Локальная матрица жесткости для кубического КЭ
    vector<vector<double> > local_matrix = {
            {a*37.0/(10.0*L) - C*8*L/105.0 +B*1.0/2.0, -a*189.0/(40.0*L) - C*33*L/560.0 - B*57/80.0, a*27.0/(20.0*L) + C*3*L/140.0 + B*3.0/10.0, -a*13.0/(40.0*L) - C*19.0*L/1680.0 - B*7/80.0},
            {-a*189.0/(40.0*L) - C*33*L/560.0 + B*57/80.0, a*54.0/(5.0*L)-C*27*L/70.0, -a*297.0/(40*L) + C*27*L/560.0 - B*81.0/80.0, a*27.0/(20.0*L) + C*3*L/140.0 + B*3.0/10.0},
            {a*27.0/(20.0*L) + C*3*L/140.0 - B*3.0/10.0, -a*297.0/(40.0*L) + C*27*L/560.0 + B*81.0/80.0, a*54.0/(5.0*L) - C*27*L/70.0, -a*189.0/(40.0*L) - C*33*L/560.0 - B*57/80.0},
            {-a*13.0/(40.0*L) - C*19.0*L/1680.0 + B*7/80.0, a*27.0/(20.0*L) + C*3*L/140.0 - B*3.0/10.0 , -a*189.0/(40.0*L) - C*33*L/560.0 + B*57/80.0, a*37.0/(10.0*L) - C*8*L/105.0 - B*1.0/2.0}
    };

    // Локальный вектор нагрузок (дополнительные слагаемые для первого и последнего элементов учитываются далее)
    vector<double> local_b = {
            D * L / 8.0,
            D*3.0 * L / 8.0,
            D*3.0 * L / 8.0,
            D * L / 8.0
    };

    // Производим матричные преобразования для обнуления элементов локальной матрицы жесткости, относящихся к внутренним узлам
    for (unsigned i = 1; i < 3; i++)
    {
        for (unsigned j = 0; j < 4; j++)
        {
            if (fabs(local_matrix.at(j).at(i)) > EPS_GAUSS && i != j)
            {
                double val = local_matrix.at(j).at(i) / local_matrix.at(i).at(i);
                local_b.at(j) -= val * local_b.at(i);
                for (unsigned k = 0; k < 4; k++)
                {
                    local_matrix.at(j).at(k) -= val *local_matrix.at(i).at(k);
                }
            }
        }
    }

    // Исключаем внутренние узлы из рассмотрения
    vector<vector<double>> local_matrix_mod = {
            {local_matrix.at(0).at(0), local_matrix.at(0).at(3)},
            {local_matrix.at(3).at(0), local_matrix.at(3).at(3)}
    };
    vector<double> local_b_mod = {local_b.at(0), local_b.at(3)};

    // Ансамблирование и получение глобальной матрицы жесткости для кубического КЭ
    for (unsigned i = 0; i < ELEMS_NUM; i++)
    {
        for (unsigned j = 0; j < 2; j++)
        {
            for (unsigned k = 0; k < 2; k++)
            {
                A.at(i + j).at(i + k) += local_matrix_mod.at(j).at(k);
            }
        }
    }
    for (unsigned i = 0; i < ELEMS_NUM; i++)
    {
        b.at(i) += local_b_mod.at(0);
        b.at(i+1) += local_b_mod.at(1);
    }

    // Учет ГУ
    // Начало интервала
    if (BEGIN_TYPE == TYPE_1)
    {
        b.at(0) = BEGIN;
        A.at(0).at(0) = 1;
        A.at(0).at(1) = 0;
    }
    else if (BEGIN_TYPE == TYPE_2)
    {
        b.at(0) = local_b_mod.at(0) - a*BEGIN;
    }
    else if (BEGIN_TYPE == TYPE_3)
    {
        b.at(0) = local_b_mod.at(0);
        A.at(0).at(0) -= a*BEGIN;
    }

    // Конец интервала
    if (END_TYPE == TYPE_1)
    {
        b.at(size-1) = END;
        A.at(size-1).at(size-1) = 1;
        A.at(size-1).at(size-2) = 0;
    }
    else if (END_TYPE == TYPE_2)
    {
        b.at(size-1) = local_b_mod.at(1) + a*END;
    }
    else if (END_TYPE == TYPE_3)
    {
        b.at(size-1) = local_b_mod.at(1);
        A.at(size-1).at(size-1) -= a*END;
    }

    // Решение полученной СЛАУ методом Гаусса
    vector<double> res = GaussSolve(A, b);
    return res;
}
